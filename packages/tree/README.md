# @texnous/tree

Structures to handle trees and tree nodes.

This package is a part of **[TeXnous project](http://texnous.org)**.

## Install

The corresponding node module can be installed with

```
npm install --save @texnous/tree
```
