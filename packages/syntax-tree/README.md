# @texnous/syntax-tree

Structures to handle syntax trees and tokens.

This package is a part of **[TeXnous project](http://texnous.org)**.

## Install

The corresponding node module can be installed with

```
npm install --save @texnous/syntax-tree
```
