# @texnous/parser

Base class to parse formatted text documents.

This package is a part of **[TeXnous project](http://texnous.org)**.

## Install

The corresponding node module can be installed with

```
npm install --save @texnous/parser
```
