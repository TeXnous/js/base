# Base TeXnous structures and classes

This repository provides the following packages:

- [@texnous/tree](packages/tree);
- [@texnous/syntax-tree](packages/syntax-tree);
- [@texnous/parser](packages/parser).

## Developer scripts

The following scripts could be used to work with the source code:

- `npm run lint` to call the linter.
- `npm run test` to call the unit tests.
