/**
 * @file Tests for syntax tree structure elements.
 * This file is a part of TeXnous project.
 *
 * @copyright TeXnous Team 2016
 * Contact: http://texnous.org <team@texnous.org>
 *
 * @author Kirill Chuvilin <k.chuvilin@texnous.org>
 *
 * @license BSD-3-Clause
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL TEXNOUS TEAM OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

'use strict'

const SyntaxTree = require('../packages/syntax-tree/src') // syntax tree structure elements

let rootToken, token1, token2, token3, token4, token5, token6

/**
 * Tests of syntax tree structure elements
 * @author Kirill Chuvilin <k.chuvilin@texnous.org>
 */
module.exports = {
  Token: {
    constructor: test => {
      test.doesNotThrow(() => { token1 = new SyntaxTree.Token({}) })
      test.doesNotThrow(() => { token3 = new SyntaxTree.Token({}) })
      test.doesNotThrow(() => { token2 = new SyntaxTree.Token({ parentToken: token3 }) })
      test.throws(() => new SyntaxTree.Token({ childTokens: [token2] }))
      test.doesNotThrow(() => { token4 = new SyntaxTree.Token({}) })
      test.doesNotThrow(() => { token5 = new SyntaxTree.Token({}) })
      test.doesNotThrow(() => { token6 = new SyntaxTree.Token({}) })
      test.doesNotThrow(() => { rootToken = new SyntaxTree.Token({ childTokens: [token1, token5] }) })
      test.done()
    },
    insertChildToken: test => {
      test.doesNotThrow(() => rootToken.insertChildToken(token4, 1))
      test.throws(() => rootToken.insertChildToken(token3, 1))
      test.doesNotThrow(() => rootToken.insertChildToken(token6, 1, 2))
      test.done()
    },
    insertChildSubtree: test => {
      test.doesNotThrow(() => rootToken.insertChildSubtree(token3, 1))
      test.done()
    },
    childToken: test => {
      test.equal(rootToken.childToken(0), token1)
      test.equal(token6.childToken(1), token5)
      test.equal(token6.childToken(2), null)
      test.equal(token1.childToken(1), null)
      test.done()
    },
    childTokens: test => {
      test.deepEqual(rootToken.childTokens, [token1, token3, token6])
      test.deepEqual(token1.childTokens, [])
      test.deepEqual(token3.childTokens, [token2])
      test.deepEqual(token6.childTokens, [token4, token5])
      test.done()
    },
    toString: test => {
      test.equal(
        rootToken.toString(),
        'SyntaxTree.Token [ SyntaxTree.Token [], SyntaxTree.Token [ SyntaxTree.Token [] ], SyntaxTree.Token [ SyntaxTree.Token [], SyntaxTree.Token [] ] ]'
      )
      // noinspection JSCheckFunctionSignatures
      test.equal(
        rootToken.toString(undefined, 2),
        'SyntaxTree.Token [ SyntaxTree.Token [], SyntaxTree.Token [ ... ], SyntaxTree.Token [ ... ] ]'
      )
      // noinspection JSCheckFunctionSignatures
      test.equal(rootToken.toString(true), 'SyntaxTree.Token { "" }')
      // noinspection JSCheckFunctionSignatures
      test.equal(rootToken.toString(false), '')
      // noinspection JSCheckFunctionSignatures
      test.equal(
        rootToken.toString('  '),
        'SyntaxTree.Token [\n' +
        '  SyntaxTree.Token [],\n' +
        '  SyntaxTree.Token [\n' +
        '    SyntaxTree.Token []\n' +
        '  ],\n' +
        '  SyntaxTree.Token [\n' +
        '    SyntaxTree.Token [],\n' +
        '    SyntaxTree.Token []\n' +
        '  ]\n' +
        ']'
      )
      // noinspection JSCheckFunctionSignatures
      test.equal(
        rootToken.toString('  ', 2),
        'SyntaxTree.Token [\n' +
        '  SyntaxTree.Token [],\n' +
        '  SyntaxTree.Token [ ... ],\n' +
        '  SyntaxTree.Token [ ... ]\n' +
        ']'
      )
      test.done()
    }
  },
  SyntaxTree: {
    constructor: test => {
      // noinspection JSCheckFunctionSignatures
      test.throws(() => new SyntaxTree())
      // noinspection JSCheckFunctionSignatures
      test.throws(() => new SyntaxTree(rootToken))
      test.doesNotThrow(() => new SyntaxTree('source', rootToken))
      test.done()
    }
  }
}
