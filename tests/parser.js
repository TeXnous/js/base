/**
 * @file Tests for formatted text parser base.
 * This file is a part of TeXnous project.
 *
 * @copyright TeXnous Team 2017
 * Contact: http://texnous.org <team@texnous.org>
 *
 * @author Kirill Chuvilin <k.chuvilin@texnous.org>
 *
 * @license BSD-3-Clause
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL TEXNOUS TEAM OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

'use strict'

const Parser = require('../packages/parser/src') // formatted text parser base class

let state, context

/**
 * Tests of formatted text parser base class
 * @author Kirill Chuvilin <kirill.chuvilin@gmail.com>
 */
module.exports = {
  State: {
    constructor: test => {
      test.doesNotThrow(() => { state = new Parser.State() })
      test.doesNotThrow(() => new Parser.State(state))
      test.done()
    },
    copy: test => {
      let stateCopy
      test.doesNotThrow(() => { stateCopy = state.copy() })
      // noinspection JSUnusedAssignment
      test.equal(state.copy(stateCopy), stateCopy)
      test.done()
    }
  },
  Context: {
    constructor: test => {
      test.doesNotThrow(() => { context = new Parser.Context('test source') })
      test.doesNotThrow(() => new Parser.Context(context))
      test.done()
    },
    copy: test => {
      let contextCopy
      test.doesNotThrow(() => { contextCopy = context.copy() })
      // noinspection JSUnusedAssignment
      test.equal(context.copy(contextCopy), contextCopy)
      test.done()
    },
    source: test => {
      test.equal(context.source, 'test source')
      test.equal(context.sourceOffset, 0)
      test.equal(context.sourceCuttingOffset, 0)
      test.equal(context.sourceCharacter, 't')
      test.equal(context.isAtSourceEnd(), false)
      test.equal(context.sourceProbe('test'), true)
      test.equal(context.sourceProbe('source'), false)
      test.equal(context.sourceProbe(/^source/), false)
      test.equal(context.sourceProbe(/.*source/), true)
      const matchResult = ['source']
      matchResult.groups = undefined
      matchResult.index = 5
      matchResult.input = context.source
      // test.deepEqual(context.sourceMatch(/source/), matchResult);
      test.equal(context.sourceProbe(/^test/, true), true)
      test.equal(context.sourceProbe(/^\ssource/), true)
      test.equal(context.sourceOffset, 4)
      test.equal(context.sourceCuttingOffset, 0)
      test.doesNotThrow(() => context.markSourceCuttingOffset())
      test.equal(context.sourceCuttingOffset, 4)
      test.done()
    }
  },
  Parser: {
    constructor: test => {
      test.doesNotThrow(() => new Parser())
      test.done()
    }
  }
}
