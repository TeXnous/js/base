/**
 * @file Tests for tree structure elements.
 * This file is a part of TeXnous project.
 *
 * @copyright TeXnous Team 2017
 * Contact: http://texnous.org <team@texnous.org>
 *
 * @author Kirill Chuvilin <k.chuvilin@texnous.org>
 *
 * @license BSD-3-Clause
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL TEXNOUS TEAM OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

'use strict'

const Tree = require('../packages/tree/src') // tree structure elements

let rootNode, node1, node2, node3, node4, node5, node6
let tree
let nodesEnumeration

/**
 * Tests of tree structure elements
 * @author Kirill Chuvilin <k.chuvilin@texnous.org>
 */
module.exports = {
  Node: {
    constructor: test => {
      test.doesNotThrow(() => { node1 = new Tree.Node({ }) })
      test.doesNotThrow(() => { node3 = new Tree.Node({ }) })
      test.doesNotThrow(() => { node2 = new Tree.Node({ parentNode: node3 }) })
      test.throws(() => new Tree.Node({ childNodes: [node2] }))
      test.doesNotThrow(() => { node4 = new Tree.Node({}) })
      test.doesNotThrow(() => { node5 = new Tree.Node({}) })
      test.doesNotThrow(() => { node6 = new Tree.Node({}) })
      test.doesNotThrow(() => { rootNode = new Tree.Node({ childNodes: [node1, node5] }) })
      test.done()
    },
    insertChildNode: test => {
      test.doesNotThrow(() => rootNode.insertChildNode(node4, 1))
      test.throws(() => rootNode.insertChildNode(node3, 1))
      test.doesNotThrow(() => rootNode.insertChildNode(node6, 1, 2))
      test.done()
    },
    insertChildSubtree: test => {
      test.doesNotThrow(() => rootNode.insertChildSubtree(node3, 1))
      test.done()
    },
    childNode: test => {
      test.equal(rootNode.childNode(0), node1)
      test.equal(node6.childNode(1), node5)
      test.equal(node6.childNode(2), null)
      test.equal(node1.childNode(1), null)
      test.done()
    },
    childNodes: test => {
      test.deepEqual(rootNode.childNodes, [node1, node3, node6])
      test.deepEqual(node1.childNodes, [])
      test.deepEqual(node3.childNodes, [node2])
      test.deepEqual(node6.childNodes, [node4, node5])
      test.done()
    },
    toString: test => {
      test.equal(
        rootNode.toString(),
        'Tree.Node [ Tree.Node [], Tree.Node [ Tree.Node [] ], Tree.Node [ Tree.Node [], Tree.Node [] ] ]'
      )
      // noinspection JSCheckFunctionSignatures
      test.equal(
        rootNode.toString(undefined, 2),
        'Tree.Node [ Tree.Node [], Tree.Node [ ... ], Tree.Node [ ... ] ]'
      )
      // noinspection JSCheckFunctionSignatures
      test.equal(rootNode.toString(true), 'Tree.Node {  }')
      // noinspection JSCheckFunctionSignatures
      test.equal(rootNode.toString(false), '')
      // noinspection JSCheckFunctionSignatures
      test.equal(
        rootNode.toString('  '),
        'Tree.Node [\n' +
        '  Tree.Node [],\n' +
        '  Tree.Node [\n' +
        '    Tree.Node []\n' +
        '  ],\n' +
        '  Tree.Node [\n' +
        '    Tree.Node [],\n' +
        '    Tree.Node []\n' +
        '  ]\n' +
        ']'
      )
      // noinspection JSCheckFunctionSignatures
      test.equal(
        rootNode.toString('  ', 2),
        'Tree.Node [\n' +
        '  Tree.Node [],\n' +
        '  Tree.Node [ ... ],\n' +
        '  Tree.Node [ ... ]\n' +
        ']'
      )
      test.done()
    }
  },
  Tree: {
    constructor: test => {
      // noinspection JSCheckFunctionSignatures
      test.throws(() => new Tree())
      test.doesNotThrow(() => { tree = new Tree(rootNode) })
      test.done()
    },
    enumerateNodes: test => {
      test.doesNotThrow(() => {
        nodesEnumeration = tree.enumerateNodes({
          nodes: true,
          nodeIndices: true,
          keyRoots: true,
          keyRootIndices: true,
          mostLeftChildNodes: true,
          mostLeftChildNodeIndices: true
        })
      })
      test.deepEqual(nodesEnumeration.nodes, [null, node1, node2, node3, node4, node5, node6, rootNode])
      test.deepEqual(nodesEnumeration.keyRoots, [node3, node5, node6, rootNode])
      test.deepEqual(nodesEnumeration.keyRootIndices, [3, 5, 6, 7])
      test.deepEqual(nodesEnumeration.keyRootIndices, [3, 5, 6, 7])
      test.deepEqual(nodesEnumeration.mostLeftChildNodeIndices, [0, 1, 2, 2, 4, 5, 4, 1])
      test.done()
    }
  }
}
